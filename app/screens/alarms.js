import React from 'react'
import { StyleSheet, Text, View, ScrollView } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'

import { AlarmListItem } from '../components/alarm-list-item'

export default class Alarms extends React.Component {
  static navigationOptions = {
    title: 'Alarms',
    tabBarIcon: ({ tintColor }) => <MaterialIcons name="alarm" size={28} color={tintColor} />
  }

  render () {
    return (
      <ScrollView>
        <AlarmListItem name='Alarm 1' time='11:15' isOn={true} />
        <AlarmListItem name='This is a very long name for an alarm' time='11:15' isOn={false} />
        <AlarmListItem name='Alarm 3' time='11:15' isOn={false} />
        <AlarmListItem name='Alarm 4' time='11:15' isOn={false} />
        <AlarmListItem name='Alarm 5' time='11:15' isOn={true} />
        <AlarmListItem name='Alarm 6' time='11:15' isOn={true} />
        <AlarmListItem name='Alarm 7' time='11:15' isOn={true} />
        <AlarmListItem name='Alarm 8' time='11:15' isOn={true} />
        <AlarmListItem name='Alarm 9' time='11:15' isOn={true} />
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    fontSize: 25,
    textAlign: 'center'
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44
  }
})
