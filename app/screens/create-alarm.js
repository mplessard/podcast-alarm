import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'

export default class CreateAlarm extends React.Component {
  static navigationOptions = {
    title: 'Create alarm',
    tabBarIcon: ({ tintColor }) => <MaterialIcons name="add-alarm"size={28} color={tintColor} />
  }

  render () {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Create alarm</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
  title: {
    fontSize: 25,
    textAlign: 'center'
  }
})
