import React from 'react'
import { StyleSheet, Text, View, Switch } from 'react-native'

export class AlarmListItem extends React.Component {
  render () {
    const name = this.props.name
    const time = this.props.time
    const isOn = this.props.isOn
    return (
      <View style={styles.container}>
        <View style={styles.timeNameContainer}>
          <Text style={styles.time}>{ time }</Text>
          <Text numberOfLines={1} style={styles.name}>{ name }</Text>
        </View>
        <View style={styles.isOncontainer}>
          <Switch value={ isOn }/>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderColor: '#e0e0e0'
  },
  timeNameContainer: {
    flex: 7,
    flexDirection: 'column',
    padding: 10
  },
  time: {
    fontSize: 25
  },
  name: {
    fontSize: 18,
    fontStyle: 'italic',
    flex: 1
  },
  isOncontainer: {
    flex: 1,
    padding: 10,
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  isOn: {
  }
})
