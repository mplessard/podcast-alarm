export default DEFAULT_NAVIGATION_OPTIONS = {
  title: 'Podcast Alarm',
  headerStyle: {
    backgroundColor: '#38618c'
  },
  headerTintColor: '#ffffff'
}
