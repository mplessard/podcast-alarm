import React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation'

import DEFAULT_NAVIGATION_OPTIONS from './app/config'
import Alarms from './app/screens/alarms'
import CreateAlarm from './app/screens/create-alarm'

const BottomTabNavigator = createBottomTabNavigator({
  Alarms: {
    screen: Alarms
  },
  CreateAlarm: {
    screen: CreateAlarm
  }
}, {
  initialRouteName: 'Alarms'
})

const StackNavigator = createStackNavigator({
  BottomTabNavigator: {
    screen: BottomTabNavigator
  }
},
{
  navigationOptions: DEFAULT_NAVIGATION_OPTIONS
})

export default class App extends React.Component {
  render () {
    return (
      <StackNavigator />
    )
  }
}
